<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use DateTime;
use DateTimeImmutable;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ApiResource(mercure: true)]
#[ORM\Entity]
class EscapeGame 
{
    #[ORM\Id]
    #[ORM\Column(type: Types::INTEGER)]
    #[ORM\GeneratedValue]
    private ?string $id = null;

    #[ORM\Column(type: 'string', length: 100)]
    public string $name = '';

    #[ORM\Column(type: 'text')]
    public string $description = '';

    #[ORM\Column(type: 'integer')]
    public int $minSize = 1;

    #[ORM\Column(type: 'integer')]
    public int $maxSize = 6;

    #[ORM\Column(type: 'json')]
    public array $timeSlots = [];
    
    #[ORM\Column(type: 'json')]
    public array $priceList = [];

    #[ORM\OneToMany(targetEntity: "Booking", mappedBy: "escapeGame")]
    private $bookings;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE)]
    public ?DateTimeImmutable $creationDatetime;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    public ?DateTime $updateDatetime;

    public function __construct()
    {
        $this->creationDatetime = new DateTimeImmutable();
    }

    public function getId() 
    {
        return $this->id;
    }

    public function getBookings() 
    {
        return $this->bookings;
    }
}