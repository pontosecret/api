<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controntoller\BookingController;
use DateTime;
use DateTimeImmutable;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(mercure: true)]
#[ORM\Entity]
class Booking
{
    #[ORM\Id]
    #[ORM\Column(type: Types::INTEGER)]
    #[ORM\GeneratedValue]
    private ?string $id = null;

    #[ORM\ManyToOne(targetEntity: "Customer", inversedBy: "bookings")]
    #[Assert\NotBlank]
    public ?Customer $customer;

    #[ORM\ManyToOne(targetEntity: "EscapeGame", inversedBy: "bookings")]
    #[Assert\NotBlank]
    public ?EscapeGame $escapeGame;

    #[ORM\Column(type: Types::INTEGER, length: 2)]
    #[Assert\NotBlank]
    public int $nbPlayersAdults;

    #[ORM\Column(type: Types::INTEGER, length: 2)]
    #[Assert\NotBlank]
    public int $nbPlayersChildren;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Assert\NotBlank]
    public DateTime $date;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE)]
    #[Assert\NotBlank]
    private ?DateTimeImmutable $creationDatetime;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTime $updateDatetime;

    public function __construct()
    {
        $this->creationDatetime = new DateTime();
    }

    public function getId() 
    {
        return $this->id;
    }

    public function getCreationDatetime() 
    {
        return $this->creationDatetime;
    }

    public function getUpdateDatetime() 
    {
        return $this->updateDatetime;
    }
}