<?php

namespace App\Entity;

use DateTime;
use DateTimeImmutable;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\Column(type: Types::INTEGER)]
    #[ORM\GeneratedValue]
    private ?string $id = null;

    #[ORM\Column(type: Types::STRING, length: 80, unique: true)]
    #[Assert\NotBlank]
    private $email;

    #[ORM\Column(type: Types::JSON)]
    public $roles = [];

    #[ORM\Column(type: Types::STRING)]
    private string $salt;

    #[ORM\Column(type: Types::STRING)]
    private string $hashedPassword;

    public ?string $password;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE)]
    #[Assert\NotBlank]
    private ?DateTimeImmutable $creationDatetime;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTime $updateDatetime;

    public function __construct()
    {
        $this->creationDatetime = new DateTimeImmutable();
    }

    public function getId() 
    {
        return $this->id;
    }

    public function getCreationDatetime() 
    {
        return $this->creationDatetime;
    }

    public function getUpdateDatetime() 
    {
        return $this->updateDatetime;
    }

    public function getUserIdentifier() 
    {
        return $this->email;
    } 
    
    public function getRoles() 
    {
        return $this->roles ?? ['ROLE_USER'];
    }
    
    public function getPassword(): ?string
    {
        return $this->hashedPassword;
    }
    
    public function setPassword(string $password): User
    {
        $this->hashedPassword = $password;

        return $this;
    }
    
    public function getSalt()
    {
        return $this->salt;
    }
    
    public function eraseCredentials() 
    {
        $this->password = null;

        return $this;
    }
    
    public function getUsername() 
    {
        return $this->getUserIdentifier();
    }

    public function setEmail($email) 
    {
        $this->email = $email;

        return $this;
    }

    public function setRawPassword($password) 
    {
        $this->password = $password;

        return $this;
    }

    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }
}