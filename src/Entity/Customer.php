<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(mercure: true)]
#[ORM\Entity]
class Customer 
{
    #[ORM\Id]
    #[ORM\Column(type: Types::INTEGER)]
    #[ORM\GeneratedValue]
    private ?string $id = null;

    #[ORM\Column]
    #[Assert\NotBlank]
    public string $title = '';

    #[ORM\Column]
    #[Assert\NotBlank]
    public string $lastname = '';

    #[ORM\Column]
    #[Assert\NotBlank]
    public string $firstname = '';

    #[ORM\OneToMany(targetEntity: "Booking", mappedBy: "customer")]
    #[Assert\NotBlank]
    private $bookings;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE)]
    #[Assert\NotBlank]
    public ?DateTime $creationDatetime;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    public ?DateTime $updateDatetime;

    public function __construct()
    {
        $this->creationDatetime = new DateTime();
        $this->bookings = new ArrayCollection();
    }

    public function getId() 
    {
        return $this->id;
    }

    public function getBookings() 
    {
        return $this->bookings;
    }
}