<?php

namespace App\Command;

use App\Entity\User;
use App\Service\UserManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateUserCommand extends Command
{
    protected static $defaultName = 'app:create-user';
    
    private $userManager;

    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setHelp('This command allows you to create a user...')
            ->addArgument('email')
            ->addArgument('password')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->userManager->create(
            $input->getArgument('email'), 
            $input->getArgument('password')
        );

        $output->writeln('User successfully generated!');

        return Command::SUCCESS;
    }
}