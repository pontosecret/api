<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211101170538 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function isTransactional(): bool
    {
        return false;
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE book CHANGE update_datetime update_datetime DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE booking CHANGE update_datetime update_datetime DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE customer CHANGE update_datetime update_datetime DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE escape_game ADD creation_datetime DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', ADD update_datetime DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE book CHANGE update_datetime update_datetime DATETIME NOT NULL');
        $this->addSql('ALTER TABLE booking CHANGE update_datetime update_datetime DATETIME NOT NULL');
        $this->addSql('ALTER TABLE customer CHANGE update_datetime update_datetime DATETIME NOT NULL');
        $this->addSql('ALTER TABLE escape_game DROP creation_datetime, DROP update_datetime');
    }
}
